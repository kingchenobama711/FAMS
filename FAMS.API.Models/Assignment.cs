﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FAMS.API.Models;

public class Assignment
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }

    public int ModuleId { get; set; }

    [Required]
    [StringLength(255)]
    public required string AssignmentName { get; set; }

    [StringLength(255)]
    public string? AssignmentType { get; set; } //TODO: unknown 

    public DateTime DueDate { get; set; }

    [StringLength(255)]
    public string? Description { get; set; }

    public DateTime CreatedDate { get; set; }

    public int CreatedUserId { get; set; }

    public DateTime? UpdatedDate { get; set; }

    public int? UpdatedUserId { get; set; }

    //navigator
    public virtual User CreatedUser { get; set; } = null!;
    public virtual Module Module { get; set; } = null!;
    public virtual User? UpdatedUser { get; set; }
    public virtual ICollection<Score> Scores { get; set; } = new List<Score>();
}