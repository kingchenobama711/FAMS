﻿using FAMS.API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FAMS.API.DataAccess.Configurations
{
    public class AssignmentConfiguration : IEntityTypeConfiguration<Assignment>
    {
        public void Configure(EntityTypeBuilder<Assignment> builder)
        {
            builder.HasOne(d => d.Module).WithMany(p => p.Assignments)
                .HasForeignKey(d => d.ModuleId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasData(
                new Assignment
                {
                    Id = 1,
                    ModuleId = 2,
                    AssignmentName = "Quiz fams 2_1",
                    AssignmentType = "",
                    Description = "Exam 1 for project fams 2",
                    CreatedUserId = 2,
                },
                new Assignment
                {
                    Id = 2,
                    ModuleId = 3,
                    AssignmentName = "Quiz UI_1",
                    AssignmentType = "",
                    Description = "Exam 1 for UI",
                    CreatedUserId = 3,
                },
                new Assignment
                {
                    Id = 3,
                    ModuleId = 4,
                    AssignmentName = "Quiz API_1",
                    AssignmentType = "",
                    Description = "Exam 1 for API",
                    CreatedUserId = 4,
                },
                new Assignment
                {
                    Id = 4,
                    ModuleId = 5,
                    AssignmentName = "Quiz SAP_1",
                    AssignmentType = "",
                    Description = "Exam 1 for SAP",
                    CreatedUserId = 5,
                },
                new Assignment
                {
                    Id = 5,
                    ModuleId = 6,
                    AssignmentName = "Quiz Test_1",
                    AssignmentType = "",
                    Description = "Exam 1  for Test",
                    CreatedUserId = 6,
                },
                new Assignment
                {
                    Id = 6,
                    ModuleId = 7,
                    AssignmentName = "Quiz SAP 2_1",
                    AssignmentType = "",
                    Description = "Exam 1 for SAP 2",
                    CreatedUserId = 1,
                },
                new Assignment
                {
                    Id = 7,
                    ModuleId = 8,
                    AssignmentName = "Quiz Test 2_1",
                    AssignmentType = "",
                    Description = "Exam 1 for Test 2",
                    CreatedUserId = 2,
                },
                new Assignment
                {
                    Id = 8,
                    ModuleId = 1,
                    AssignmentName = "Quiz fams 1_1",
                    AssignmentType = "",
                    Description = "Exam 1 for project fams 1",
                    CreatedUserId = 3,
                },
                new Assignment
                {
                    Id = 9,
                    ModuleId = 2,
                    AssignmentName = "Quiz fams 2_2",
                    AssignmentType = "",
                    Description = "Exam 2 for project fams 2",
                    CreatedUserId = 4,
                },
                new Assignment
                {
                    Id = 10,
                    ModuleId = 3,
                    AssignmentName = "Quiz UI_2",
                    AssignmentType = "",
                    Description = "Exam 2 for UI",
                    CreatedUserId = 5,
                }
                );
        }
    }
}
