﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FAMS.API.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class UpdateCreatedAndUpdatedClassModel : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "UpdatedById",
                table: "Classes",
                newName: "UpdatedUserId");

            migrationBuilder.RenameColumn(
                name: "CreatedById",
                table: "Classes",
                newName: "CreatedUserId");

            migrationBuilder.AlterColumn<string>(
                name: "Status",
                table: "Classes",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "Active",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Classes",
                type: "datetime2",
                nullable: false,
                defaultValueSql: "GETDATE()",
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.CreateIndex(
                name: "IX_Classes_CreatedUserId",
                table: "Classes",
                column: "CreatedUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Classes_UpdatedUserId",
                table: "Classes",
                column: "UpdatedUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Classes_Users_CreatedUserId",
                table: "Classes",
                column: "CreatedUserId",
                principalTable: "Users",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Classes_Users_UpdatedUserId",
                table: "Classes",
                column: "UpdatedUserId",
                principalTable: "Users",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Classes_Users_CreatedUserId",
                table: "Classes");

            migrationBuilder.DropForeignKey(
                name: "FK_Classes_Users_UpdatedUserId",
                table: "Classes");

            migrationBuilder.DropIndex(
                name: "IX_Classes_CreatedUserId",
                table: "Classes");

            migrationBuilder.DropIndex(
                name: "IX_Classes_UpdatedUserId",
                table: "Classes");

            migrationBuilder.RenameColumn(
                name: "UpdatedUserId",
                table: "Classes",
                newName: "UpdatedById");

            migrationBuilder.RenameColumn(
                name: "CreatedUserId",
                table: "Classes",
                newName: "CreatedById");

            migrationBuilder.AlterColumn<string>(
                name: "Status",
                table: "Classes",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldDefaultValue: "Active");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Classes",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValueSql: "GETDATE()");
        }
    }
}
