﻿using FAMS.API.Models;

namespace FAMS.API.BusinessLogic.Interfaces
{
    public interface IClassService
    {
        Task<Class?> GetClassDetailById(int id);
    }
}
