﻿namespace FAMS.API.Helper.Dtos.Students
{
    public class QueryStudentsDto
    {
        public int PageSize = 10;
        public int PageNumber = 1;
        public string? SearchQuery;
    }
}
