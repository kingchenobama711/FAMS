﻿using FAMS.API.DataAccess.Interfaces;
using FAMS.API.Models;

namespace FAMS.API.DataAccess.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(FamsDbContext context) : base(context)
        {
        }
    }
}
