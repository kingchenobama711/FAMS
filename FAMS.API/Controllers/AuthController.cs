﻿using AutoMapper;
using FAMS.API.BusinessLogic.Interfaces;
using FAMS.API.Helper.Dtos.Auth;
using FAMS.API.Helper.Dtos.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FAMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;
        private readonly IMapper _mapper;

        public AuthController(IAuthService authService, IMapper mapper)
        {
            _authService = authService;
            _mapper = mapper;
        }

        [HttpPost("login")]
        public async Task<ActionResult<AuthResponseDto>> Login(LoginDto body)
        {
            return await _authService.Login(body);
        }

        [HttpPost("register")]
        public async Task<ActionResult> Register(RegisterDto body)
        {
            await _authService.Register(body);
            return Ok();
        }

        [HttpGet("who-am-i"), Authorize]
        public async Task<ActionResult<GetUserDto>> WhoAmI()
        {
            var user = await _authService.GetUserByClaims(HttpContext.User);
            return _mapper.Map<GetUserDto>(user);
        }
    }
}

