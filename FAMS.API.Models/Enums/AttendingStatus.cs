﻿namespace FAMS.API.Models.Enums;

public enum AttendingStatus
{
    InClass,
    Reserve,
    Finish,
    DropOut
}