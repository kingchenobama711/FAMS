﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace FAMS.API.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class SeedData : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Modules",
                columns: new[] { "Id", "CreatedUserId", "ModuleName", "UpdatedDate", "UpdatedUserId" },
                values: new object[,]
                {
                    { 3, 1, "Todo list UI", null, null },
                    { 5, 1, "Do SAP", null, null },
                    { 6, 1, "Do Test", null, null },
                    { 7, 1, "Do SAP 2", null, null }
                });

            migrationBuilder.InsertData(
                table: "Programs",
                columns: new[] { "Id", "Code", "CreatedUserId", "Duration", "ProgramName", "Status", "UpdatedDate", "UpdatedUserId" },
                values: new object[,]
                {
                    { 1, "NET", 1, null, "ASP .NET Core for FA student", "Active", null, null },
                    { 3, "Test", 1, null, "Test for FA student", "Active", null, null }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Address", "Dob", "Email", "FullName", "Gender", "Password", "Phone", "Role", "Username" },
                values: new object[,]
                {
                    { 4, null, null, "classadmin2@gmail.com", null, "Male", "548d8cf86e2d301f6e1f5dc621cba2e409e8e814ba35ca1feeff6b0b995d848f", null, "ClassAdmin", null },
                    { 5, null, null, "trainer2@gmail.com", null, "Female", "548d8cf86e2d301f6e1f5dc621cba2e409e8e814ba35ca1feeff6b0b995d848f", null, "Trainer", null },
                    { 6, null, null, "superadmin2@gmail.com", null, "Female", "548d8cf86e2d301f6e1f5dc621cba2e409e8e814ba35ca1feeff6b0b995d848f", null, "SuperAdmin", null }
                });

            migrationBuilder.InsertData(
                table: "Assignment",
                columns: new[] { "Id", "AssignmentName", "AssignmentType", "CreatedDate", "CreatedUserId", "Description", "DueDate", "ModuleId", "UpdatedDate", "UpdatedUserId" },
                values: new object[,]
                {
                    { 2, "Quiz UI_1", "", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, "Exam 1 for UI", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, null, null },
                    { 4, "Quiz SAP_1", "", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 5, "Exam 1 for SAP", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 5, null, null },
                    { 5, "Quiz Test_1", "", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 6, "Exam 1  for Test", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 6, null, null },
                    { 6, "Quiz SAP 2_1", "", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, "Exam 1 for SAP 2", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 7, null, null },
                    { 10, "Quiz UI_2", "", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 5, "Exam 2 for UI", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, null, null }
                });

            migrationBuilder.InsertData(
                table: "Classes",
                columns: new[] { "Id", "ClassName", "CreatedUserId", "Duration", "EndDate", "Location", "ProgramId", "StartDate", "Status", "UpdatedDate", "UpdatedUserId" },
                values: new object[,]
                {
                    { 1, "NET_01", 1, null, null, null, 1, null, "Active", null, null },
                    { 3, "NET_02", 6, null, null, null, 1, null, "Active", null, null },
                    { 5, "Test_01", 6, null, null, null, 3, null, "Active", null, null }
                });

            migrationBuilder.InsertData(
                table: "Modules",
                columns: new[] { "Id", "CreatedUserId", "ModuleName", "UpdatedDate", "UpdatedUserId" },
                values: new object[,]
                {
                    { 1, 6, "Project Fams 1", null, null },
                    { 2, 6, "Project Fams 2", null, null },
                    { 4, 6, "Todo list API", null, null },
                    { 8, 6, "Do Test 2", null, null }
                });

            migrationBuilder.InsertData(
                table: "Programs",
                columns: new[] { "Id", "Code", "CreatedUserId", "Duration", "ProgramName", "Status", "UpdatedDate", "UpdatedUserId" },
                values: new object[,]
                {
                    { 2, "React", 6, null, "React for FA student", "Active", null, null },
                    { 4, "SAP", 6, null, "SAP for FA student", "Active", null, null }
                });

            migrationBuilder.InsertData(
                table: "Assignment",
                columns: new[] { "Id", "AssignmentName", "AssignmentType", "CreatedDate", "CreatedUserId", "Description", "DueDate", "ModuleId", "UpdatedDate", "UpdatedUserId" },
                values: new object[,]
                {
                    { 1, "Quiz fams 2_1", "", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, "Exam 1 for project fams 2", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, null, null },
                    { 3, "Quiz API_1", "", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 4, "Exam 1 for API", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 4, null, null },
                    { 7, "Quiz Test 2_1", "", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, "Exam 1 for Test 2", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 8, null, null },
                    { 8, "Quiz fams 1_1", "", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, "Exam 1 for project fams 1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, null, null },
                    { 9, "Quiz fams 2_2", "", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 4, "Exam 2 for project fams 2", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, null, null }
                });

            migrationBuilder.InsertData(
                table: "Classes",
                columns: new[] { "Id", "ClassName", "CreatedUserId", "Duration", "EndDate", "Location", "ProgramId", "StartDate", "Status", "UpdatedDate", "UpdatedUserId" },
                values: new object[,]
                {
                    { 2, "React_01", 1, null, null, null, 2, null, "Active", null, null },
                    { 4, "SAP_01", 1, null, null, null, 4, null, "Active", null, null },
                    { 6, "React_02", 1, null, null, null, 2, null, "Active", null, null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Classes_ClassName",
                table: "Classes",
                column: "ClassName",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Classes_ClassName",
                table: "Classes");

            migrationBuilder.DeleteData(
                table: "Assignment",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Assignment",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Assignment",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Assignment",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Assignment",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Assignment",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Assignment",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Assignment",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Assignment",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Assignment",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Classes",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Classes",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Classes",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Classes",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Classes",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Classes",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Modules",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Modules",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Modules",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Modules",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Modules",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Modules",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Modules",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Modules",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Programs",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Programs",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Programs",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Programs",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6);
        }
    }
}
