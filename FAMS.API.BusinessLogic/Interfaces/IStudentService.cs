﻿using FAMS.API.Helper.Dtos.Query;
using FAMS.API.Helper.Dtos.Students;
using FAMS.API.Models;

namespace FAMS.API.BusinessLogic.Interfaces;

public interface IStudentService
{
    Task<PagedResult<Student>> GetStudents(QueryStudentsDto queryStudentsDto);
}