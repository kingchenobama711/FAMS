﻿using AutoMapper;
using FAMS.API.BusinessLogic.Interfaces;
using FAMS.API.DataAccess.Interfaces;
using FAMS.API.Helper.Dtos.Query;
using FAMS.API.Helper.Dtos.Students;
using FAMS.API.Models;

namespace FAMS.API.BusinessLogic.Services
{
    public class StudentService : IStudentService
    {
        private readonly IStudentRepository _studentRepository;

        public StudentService(IStudentRepository studentRepository, IMapper mapper)
        {
            _studentRepository = studentRepository;
        }

        public async Task<PagedResult<Student>> GetStudents(QueryStudentsDto queryStudentsDto)
        {
            return await _studentRepository.GetPagedStudents(queryStudentsDto);
        }
    }
}

