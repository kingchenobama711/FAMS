﻿using FAMS.API.Models;

namespace FAMS.API.DataAccess.Interfaces
{
    public interface IClassRepository : IGenericRepository<Class>
    {
        Task<Class?> GetClassDetailById(int id);
    }
}
