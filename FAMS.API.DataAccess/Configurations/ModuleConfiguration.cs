﻿using FAMS.API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FAMS.API.DataAccess.Configurations
{
    public class ModuleConfiguration : IEntityTypeConfiguration<Module>
    {
        public void Configure(EntityTypeBuilder<Module> builder)
        {
            builder
                .Property(e => e.CreatedDate)
                .HasDefaultValueSql("GETDATE()");

            builder.HasIndex(e => e.ModuleName)
                .IsUnique();

            builder.HasOne(d => d.CreatedUser).WithMany(p => p.CreatedModules)
                .HasForeignKey(d => d.CreatedUserId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasOne(d => d.UpdatedUser).WithMany(p => p.UpdatedModules)
                .HasForeignKey(d => d.UpdatedUserId);

            builder.HasData(
                new Module
                {
                    Id = 1,
                    ModuleName = "Project Fams 1",
                    CreatedUserId = 6
                }, new Module
                {
                    Id = 2,
                    ModuleName = "Project Fams 2",
                    CreatedUserId = 6
                }, new Module
                {
                    Id = 3,
                    ModuleName = "Todo list UI",
                    CreatedUserId = 1
                }, new Module
                {
                    Id = 4,
                    ModuleName = "Todo list API",
                    CreatedUserId = 6
                }, new Module
                {
                    Id = 5,
                    ModuleName = "Do SAP",
                    CreatedUserId = 1
                },
                new Module
                {
                    Id = 6,
                    ModuleName = "Do Test",
                    CreatedUserId = 1
                },
                new Module
                {
                    Id = 7,
                    ModuleName = "Do SAP 2",
                    CreatedUserId = 1
                },
                new Module
                {
                    Id = 8,
                    ModuleName = "Do Test 2",
                    CreatedUserId = 6
                }
                );
        }
    }
}
