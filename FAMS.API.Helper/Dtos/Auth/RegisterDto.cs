﻿using FAMS.API.Models.Enums;
using System.ComponentModel.DataAnnotations;

namespace FAMS.API.Helper.Dtos.Auth
{
    public class RegisterDto
    {
        [Required]
        [EmailAddress]
        [MaxLength(150)]
        public required string Email { get; set; }

        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&_])[A-Za-z\d@$!%*?&_]{8,}$", ErrorMessage = "Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character:")]
        public required string Password { get; set; }

        [EnumDataType(typeof(Role))]
        public required string Role { get; set; }

        [EnumDataType(typeof(Gender))]
        public required string Gender { get; set; }
    }
}
