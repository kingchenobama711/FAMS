﻿using FAMS.API.Models.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FAMS.API.Models
{
    public class EmailSend
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int SenderId { get; set; }

        public int EmailTemplateId { get; set; }

        [Required]
        public required string Content { get; set; }

        public DateTime SendDate { get; set; }

        [EnumDataType(typeof(ReceiverType))]
        public required string ReceiverType { get; set; } // TODO: unknown prop

        //navigator
        public virtual User Sender { get; set; } = null!;
        public virtual EmailTemplate EmailTemplate { get; set; } = null!;
        public virtual ICollection<Student> Students { get; set; } = null!;
    }
}
