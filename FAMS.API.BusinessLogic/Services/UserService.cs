﻿using FAMS.API.BusinessLogic.Interfaces;
using FAMS.API.DataAccess.Interfaces;
using FAMS.API.Models;

namespace FAMS.API.BusinessLogic.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<User?> GetUserById(int id)
        {
            return await _userRepository.GetByIdAsync(id);
        }

        public async Task<User?> GetUserByEmail(string email)
        {
            return await _userRepository.FindOneAsync(u => u.Email.Equals(email));
        }

        public async Task<User> CreateUser(string email, string password, string role, string gender)
        {
            return await _userRepository.AddAsync(new User()
            {
                Password = password,
                Email = email,
                Role = role,
                Gender = gender
            });
        }
    }
}
