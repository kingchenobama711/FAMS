﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace FAMS.API.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class AddEmailData : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "EmailTemplates",
                columns: new[] { "Id", "CreatedDate", "CreatedUserId", "Description", "Name", "Type", "UpdatedDate", "UpdatedUserId" },
                values: new object[,]
                {
                    { 1, new DateTime(2024, 1, 15, 0, 0, 0, 0, DateTimeKind.Local), 1, "Chào [Tên Học Viên],\n\nChúc bạn một ngày tốt lành! Tôi là [Tên Admin], Admin của Fresher Academy.\nChúng tôi rất vui vì bạn đã quyết định bảo lưu tại Fresher Academy để giữ vững kiến thức và kỹ năng của mình. Hiện tại, tôi muốn nhắc nhở bạn rằng thời hạn bảo lưu của bạn sẽ kết thúc trong vòng một tháng nữa.\nĐể giúp bạn dễ dàng quay lại và tiếp tục hành trình học tập, chúng tôi đề xuất bạn nên liên hệ với chúng tôi trước khi thời hạn bảo lưu hết hạn. Bằng cách này, chúng tôi có thể hỗ trợ bạn trong quá trình xếp lớp và đảm bảo rằng bạn sẽ có trải nghiệm học tập tốt nhất tại Fresher Academy.\nHãy liên hệ với chúng tôi qua số điện thoại: [Số Điện Thoại của Bạn]. Chúng tôi sẽ sẵn lòng hỗ trợ bạn với mọi thắc mắc và yêu cầu của bạn.\nChân thành cảm ơn sự quan tâm và cam kết của bạn đối với chương trình học tại Fresher Academy. Chúng tôi mong sớm được gặp lại bạn và chia sẻ những kiến thức mới.\nTrân trọng,\n[Tên Của Admin]\nAdmin Fresher Academy", "Reservation Notification", null, new DateTime(2024, 1, 15, 0, 0, 0, 0, DateTimeKind.Local), 1 },
                    { 2, new DateTime(2024, 1, 15, 0, 0, 0, 0, DateTimeKind.Local), 2, "Chào [Tên Học Viên],\n\nChúc bạn một ngày tốt lành! Tôi là [Tên Admin], Admin của Fresher Academy.\nTôi trân trọng thông báo kết quả học tập kỳ [Mã kỳ học] của sinh viên [Tên sinh viên]Hãy liên hệ với chúng tôi qua số điện thoại: [Số Điện Thoại của Bạn]. Chúng tôi sẽ sẵn lòng hỗ trợ bạn với mọi thắc mắc và yêu cầu của bạn.\nChân thành cảm ơn sự quan tâm và cam kết của bạn đối với chương trình học tại Fresher Academy. Chúng tôi mong sớm được gặp lại bạn và chia sẻ những kiến thức mới.\nTrân trọng,\n[Tên Của Admin]\nAdmin Fresher Academy", "Grades Notification", null, new DateTime(2024, 1, 15, 0, 0, 0, 0, DateTimeKind.Local), 2 },
                    { 3, new DateTime(2024, 1, 15, 0, 0, 0, 0, DateTimeKind.Local), 3, "Kính gửi anh/chị [tên admin], em tên là [tên sinh viên]\n\nEm viết đơn này mong được xin nghỉ lớp [Mã/Tên lớp] vào lúc [thời gian muốn nghỉ]\n\nvì lý do [Lý do nghỉ].", "Asking permission for absenting in class", null, new DateTime(2024, 1, 15, 0, 0, 0, 0, DateTimeKind.Local), 3 }
                });

            migrationBuilder.InsertData(
                table: "EmailSends",
                columns: new[] { "Id", "Content", "EmailTemplateId", "ReceiverType", "SendDate", "SenderId" },
                values: new object[] { 1, "", 1, "", new DateTime(2024, 1, 15, 0, 0, 0, 0, DateTimeKind.Local), 2 });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "EmailSends",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "EmailTemplates",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "EmailTemplates",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "EmailTemplates",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
