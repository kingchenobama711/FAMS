﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FAMS.API.Models
{
    public class StudentModule
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int StudentId { get; set; }

        public int ModuleId { get; set; }

        public double ModuleScore { get; set; }

        public string? ModuleLevel { get; set; } //TODO: unknown

        //navigator
        public virtual Student Student { get; set; } = null!;
        public virtual Module Module { get; set; } = null!;
    }
}
