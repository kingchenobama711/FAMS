﻿namespace FAMS.API.Models.Enums;

public enum StudentStatus
{
    Active,
    Inactive,
    Disable
}