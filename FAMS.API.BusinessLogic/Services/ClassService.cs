﻿using FAMS.API.BusinessLogic.Interfaces;
using FAMS.API.DataAccess.Interfaces;
using FAMS.API.Models;

namespace FAMS.API.BusinessLogic.Services
{
    public class ClassService : IClassService
    {
        private readonly IClassRepository _classRepository;

        public ClassService(IClassRepository classRepository)
        {
            _classRepository = classRepository;
        }

        public async Task<Class?> GetClassDetailById(int id)
        {
            return await _classRepository.GetClassDetailById(id);
        }
    }
}
