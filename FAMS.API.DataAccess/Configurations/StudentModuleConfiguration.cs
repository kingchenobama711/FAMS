﻿using FAMS.API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FAMS.API.DataAccess.Configurations
{
    public class StudentModuleConfiguration : IEntityTypeConfiguration<StudentModule>
    {
        public void Configure(EntityTypeBuilder<StudentModule> builder)
        {
            builder.HasData(
                new StudentModule
                {
                    Id = 1,
                    StudentId = 1,
                    ModuleId = 1,
                    ModuleScore = 7,
                    ModuleLevel = "Unknown"
                },
                new StudentModule
                {
                    Id = 2,
                    StudentId = 1,
                    ModuleId = 4,
                    ModuleScore = 9,
                    ModuleLevel = "Unknown"
                }, new StudentModule
                {
                    Id = 3,
                    StudentId = 2,
                    ModuleId = 2,
                    ModuleScore = 7.5,
                    ModuleLevel = "Unknown"
                }, new StudentModule
                {
                    Id = 4,
                    StudentId = 2,
                    ModuleId = 3,
                    ModuleScore = 6,
                    ModuleLevel = "Unknown"
                },
                new StudentModule
                {
                    Id = 5,
                    StudentId = 3,
                    ModuleId = 8,
                    ModuleScore = 6,
                    ModuleLevel = "Unknown"
                },
                new StudentModule
                {
                    Id = 6,
                    StudentId = 4,
                    ModuleId = 5,
                    ModuleScore = 4,
                    ModuleLevel = "Unknown"
                }
            );
        }
    }
}
