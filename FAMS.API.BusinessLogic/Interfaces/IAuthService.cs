﻿using FAMS.API.Helper.Dtos.Auth;
using FAMS.API.Models;
using System.Security.Claims;

namespace FAMS.API.BusinessLogic.Interfaces;

public interface IAuthService
{
    public Task<User> GetUserByClaims(ClaimsPrincipal claims);
    public Task<AuthResponseDto> Login(LoginDto body);
    public Task Register(RegisterDto body);
}