﻿using FAMS.API.Models.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FAMS.API.Models
{
    public class Program
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public required string ProgramName { get; set; }

        [EnumDataType(typeof(ProgramStatus))]
        public string Status { get; set; } = "Active";

        [Required]
        [StringLength(255)]
        public required string Code { get; set; }

        [StringLength(255)]
        public string? Duration { get; set; } //TODO: data type under consideration

        public DateTime CreatedDate { get; set; }

        public int CreatedUserId { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUserId { get; set; }

        //navigate
        public virtual User CreatedUser { get; set; } = null!;
        public virtual User? UpdatedUser { get; set; }
        public virtual ICollection<Class> Classes { get; set; } = new List<Class>();
        public virtual ICollection<Module> Modules { get; set; } = new List<Module>();
    }
}
