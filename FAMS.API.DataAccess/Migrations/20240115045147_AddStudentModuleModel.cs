﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FAMS.API.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class AddStudentModuleModel : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmailSendStudent_Students_StudentId",
                table: "EmailSendStudent");

            migrationBuilder.RenameColumn(
                name: "StudentId",
                table: "EmailSendStudent",
                newName: "ReceiverId");

            migrationBuilder.CreateTable(
                name: "StudentModule",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StudentId = table.Column<int>(type: "int", nullable: false),
                    ModuleId = table.Column<int>(type: "int", nullable: false),
                    ModuleScore = table.Column<int>(type: "int", nullable: false),
                    ModuleLevel = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentModule", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StudentModule_Modules_ModuleId",
                        column: x => x.ModuleId,
                        principalTable: "Modules",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentModule_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StudentModule_ModuleId",
                table: "StudentModule",
                column: "ModuleId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentModule_StudentId",
                table: "StudentModule",
                column: "StudentId");

            migrationBuilder.AddForeignKey(
                name: "FK_EmailSendStudent_Students_ReceiverId",
                table: "EmailSendStudent",
                column: "ReceiverId",
                principalTable: "Students",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmailSendStudent_Students_ReceiverId",
                table: "EmailSendStudent");

            migrationBuilder.DropTable(
                name: "StudentModule");

            migrationBuilder.RenameColumn(
                name: "ReceiverId",
                table: "EmailSendStudent",
                newName: "StudentId");

            migrationBuilder.AddForeignKey(
                name: "FK_EmailSendStudent_Students_StudentId",
                table: "EmailSendStudent",
                column: "StudentId",
                principalTable: "Students",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
