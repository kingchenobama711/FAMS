﻿using FAMS.API.DataAccess.Configurations;
using FAMS.API.Models;
using Microsoft.EntityFrameworkCore;

namespace FAMS.API.DataAccess
{
    public class FamsDbContext : DbContext
    {
        public FamsDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Assignment> Assignments { get; set; }
        public DbSet<Class> Classes { get; set; }
        public DbSet<EmailSend> EmailSends { get; set; }
        public DbSet<EmailTemplate> EmailTemplates { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<Program> Programs { get; set; }
        public DbSet<Score> Scores { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<StudentClass> StudentClasses { get; set; }
        public DbSet<StudentModule> StudentModules { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new AssignmentConfiguration());
            modelBuilder.ApplyConfiguration(new ClassConfiguration());
            modelBuilder.ApplyConfiguration(new EmailSendConfiguration());
            modelBuilder.ApplyConfiguration(new EmailTemplateConfiguration());
            modelBuilder.ApplyConfiguration(new ModuleConfiguration());
            modelBuilder.ApplyConfiguration(new ProgramConfiguration());
            modelBuilder.ApplyConfiguration(new ScoreConfiguration());
            modelBuilder.ApplyConfiguration(new StudentClassConfiguration());
            modelBuilder.ApplyConfiguration(new StudentConfiguration());
            modelBuilder.ApplyConfiguration(new StudentModuleConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
        }
    }
}
