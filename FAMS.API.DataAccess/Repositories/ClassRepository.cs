﻿using FAMS.API.DataAccess.Interfaces;
using FAMS.API.Models;
using Microsoft.EntityFrameworkCore;

namespace FAMS.API.DataAccess.Repositories
{
    public class ClassRepository : GenericRepository<Class>, IClassRepository
    {
        private readonly FamsDbContext _context;
        public ClassRepository(FamsDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<Class?> GetClassDetailById(int id)
        {
            return await _context.Classes
                .Include(c => c.Students)
                .Include(c => c.Managers)
                .Include(c => c.CreatedUser)
                .Include(c => c.UpdatedUser)
                .Include(c => c.Program)
                .FirstOrDefaultAsync(c => c.Id == id);
        }
    }
}
