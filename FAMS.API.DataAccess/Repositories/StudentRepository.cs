﻿using AutoMapper;
using FAMS.API.DataAccess.Interfaces;
using FAMS.API.Helper.Dtos.Query;
using FAMS.API.Helper.Dtos.Students;
using FAMS.API.Models;
using Microsoft.EntityFrameworkCore;

namespace FAMS.API.DataAccess.Repositories
{
    public class StudentRepository : GenericRepository<Student>, IStudentRepository
    {
        private readonly FamsDbContext _context;
        private readonly IMapper _mapper;
        public StudentRepository(FamsDbContext context, IMapper mapper) : base(context)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<PagedResult<Student>> GetPagedStudents(QueryStudentsDto queryStudentsDto)
        {
            var pageSize = queryStudentsDto.PageSize;
            var pageNumber = queryStudentsDto.PageNumber;
            var searchQuery = queryStudentsDto.SearchQuery;

            IQueryable<Student> getStudentsTask = _context.Students;

            //TODO: not done: handle search and filter
            if (!String.IsNullOrEmpty(searchQuery))
            {

                getStudentsTask = getStudentsTask.Where(s =>
                    s.Id.ToString().Contains(searchQuery) ||
                    (!String.IsNullOrEmpty(s.FullName) && s.FullName.ToString().Contains(searchQuery)) ||
                    s.Email.ToString().Contains(searchQuery) ||
                    s.Area.ToString().Contains(searchQuery) ||
                    s.Major.ToString().Contains(searchQuery));
            }

            //paged result
            var itemCount = (await getStudentsTask.ToListAsync()).Count();

            var students = await getStudentsTask.Skip(pageSize * (pageNumber - 1)).Take(pageSize).ToListAsync();

            return new PagedResult<Student>
            {
                Items = students,
                PageNumber = pageNumber,
                PageSize = pageSize,
                TotalCount = itemCount
            };
        }
    }
}
