﻿namespace FAMS.API.Models.Enums;

public enum Gender
{
    Male,
    Female
}