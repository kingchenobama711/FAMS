﻿using FAMS.API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FAMS.API.DataAccess.Configurations
{
    public class ScoreConfiguration : IEntityTypeConfiguration<Score>
    {
        public void Configure(EntityTypeBuilder<Score> builder)
        {
            builder.HasData(
                new Score
                {
                    Id = 1,
                    StudentId = 1,
                    AssignmentId = 1,
                    ScoreValue = 5
                },
                new Score
                {
                    Id = 2,
                    StudentId = 2,
                    AssignmentId = 2,
                    ScoreValue = 9
                },
                new Score
                {
                    Id = 3,
                    StudentId = 3,
                    AssignmentId = 3,
                    ScoreValue = 8
                }
            );
        }
    }
}
