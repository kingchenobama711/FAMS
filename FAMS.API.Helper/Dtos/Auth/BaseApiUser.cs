﻿using System.ComponentModel.DataAnnotations;

namespace FAMS.API.Dtos.ApiUser
{
    public class BaseApiUser
    {
        [Required]
        [StringLength(12, ErrorMessage = "Your Password is limited to {2} to {1} characters", MinimumLength = 8)]
        public required string FirstName { get; set; }

        [Required]
        [EmailAddress]
        [MaxLength(150)]
        public required string Email { get; set; }
    }
}
