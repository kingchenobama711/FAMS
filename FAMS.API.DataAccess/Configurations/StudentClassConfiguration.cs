﻿using FAMS.API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FAMS.API.DataAccess.Configurations
{
    public class StudentClassConfiguration : IEntityTypeConfiguration<StudentClass>
    {
        public void Configure(EntityTypeBuilder<StudentClass> builder)
        {
            builder.HasData(
                new StudentClass
                {
                    Id = 1,
                    ClassId = 1,
                    StudentId = 1,
                    AttendingStatus = "InClass",
                    Result = "NotYet",
                    CertificationStatus = "NotYet"
                },
                new StudentClass
                {
                    Id = 2,
                    ClassId = 2,
                    StudentId = 2,
                    AttendingStatus = "InClass",
                    Result = "NotYet",
                    CertificationStatus = "NotYet"
                },
                new StudentClass
                {
                    Id = 3,
                    ClassId = 5,
                    StudentId = 3,
                    AttendingStatus = "InClass",
                    Result = "NotYet",
                    CertificationStatus = "NotYet"
                },
                new StudentClass
                {
                    Id = 4,
                    ClassId = 4,
                    StudentId = 4,
                    AttendingStatus = "InClass",
                    Result = "NotYet",
                    CertificationStatus = "NotYet"
                }
                );
        }
    }
}
