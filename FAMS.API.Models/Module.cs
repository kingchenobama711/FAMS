﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FAMS.API.Models
{
    public class Module
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public required string ModuleName { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedUserId { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUserId { get; set; }

        //navigate
        public virtual User CreatedUser { get; set; } = null!;
        public virtual User? UpdatedUser { get; set; }
        public virtual ICollection<Program> Programs { get; set; } = new List<Program>();
        public virtual ICollection<StudentModule> StudentModules { get; set; } = new List<StudentModule>();
        public virtual ICollection<Assignment> Assignments { get; set; } = new List<Assignment>();
    }
}
