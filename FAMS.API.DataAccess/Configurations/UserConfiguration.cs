﻿using FAMS.API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FAMS.API.DataAccess.Configurations
{
    internal class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder
                .HasMany<Class>(s => s.ManagedClasses)
                .WithMany(c => c.Managers)
                .UsingEntity(
                    "UserClass",
                    l => l.HasOne(typeof(Class)).WithMany().HasForeignKey("ManagedClassId").HasPrincipalKey(nameof(Class.Id)),
                    r => r.HasOne(typeof(User)).WithMany().HasForeignKey("ManagerId").HasPrincipalKey(nameof(User.Id)),
                    j => j.HasKey("ManagedClassId", "ManagerId"));

            builder.HasIndex(c => c.Email).IsUnique();

            builder
                .HasData(
                    new User
                    {
                        Id = 1,
                        Email = "superadmin@gmail.com",
                        Password = "548d8cf86e2d301f6e1f5dc621cba2e409e8e814ba35ca1feeff6b0b995d848f",
                        Role = "SuperAdmin",
                        Gender = "Male"
                    },
                    new User
                    {
                        Id = 2,
                        Email = "classadmin@gmail.com",
                        Password = "548d8cf86e2d301f6e1f5dc621cba2e409e8e814ba35ca1feeff6b0b995d848f",
                        Role = "ClassAdmin",
                        Gender = "Female"
                    },
                    new User
                    {
                        Id = 3,
                        Email = "trainer@gmail.com",
                        Password = "548d8cf86e2d301f6e1f5dc621cba2e409e8e814ba35ca1feeff6b0b995d848f",
                        Role = "Trainer",
                        Gender = "Male"
                    },
                    new User
                    {
                        Id = 4,
                        Email = "classadmin2@gmail.com",
                        Password = "548d8cf86e2d301f6e1f5dc621cba2e409e8e814ba35ca1feeff6b0b995d848f",
                        Role = "ClassAdmin",
                        Gender = "Male"
                    },
                    new User
                    {
                        Id = 5,
                        Email = "trainer2@gmail.com",
                        Password = "548d8cf86e2d301f6e1f5dc621cba2e409e8e814ba35ca1feeff6b0b995d848f",
                        Role = "Trainer",
                        Gender = "Female"
                    },
                    new User
                    {
                        Id = 6,
                        Email = "superadmin2@gmail.com",
                        Password = "548d8cf86e2d301f6e1f5dc621cba2e409e8e814ba35ca1feeff6b0b995d848f",
                        Role = "SuperAdmin",
                        Gender = "Female"
                    }
                );
        }
    }
}
