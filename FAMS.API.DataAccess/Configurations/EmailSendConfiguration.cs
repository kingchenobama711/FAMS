﻿using FAMS.API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FAMS.API.DataAccess.Configurations
{
    public class EmailSendConfiguration : IEntityTypeConfiguration<EmailSend>
    {
        public void Configure(EntityTypeBuilder<EmailSend> builder)
        {
            builder.HasOne(d => d.Sender).WithMany(p => p.EmailSends)
                .HasForeignKey(d => d.SenderId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasOne(d => d.EmailTemplate).WithMany(p => p.EmailSends)
                .HasForeignKey(d => d.EmailTemplateId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder
                .HasMany<Student>(s => s.Students)
                .WithMany(c => c.EmailSends)
                .UsingEntity(
                    "EmailSendStudent",
                    l => l.HasOne(typeof(Student)).WithMany().HasForeignKey("ReceiverId").HasPrincipalKey(nameof(Student.Id)),
                    r => r.HasOne(typeof(EmailSend)).WithMany().HasForeignKey("EmailSendId").HasPrincipalKey(nameof(EmailSend.Id)),
                    j => j.HasKey("ReceiverId", "EmailSendId"));

            builder.HasData(new EmailSend
            {
                Id = 1,
                SenderId = 2,
                EmailTemplateId = 1,
                Content = "",
                SendDate = DateTime.Today,
                ReceiverType = string.Empty,

            }
            ,
            new EmailSend
            {
                Id = 2,
                SenderId = 1,
                EmailTemplateId = 3,
                Content = "",
                SendDate = DateTime.Today,
                ReceiverType = string.Empty,

            }
            );
        }
    }
}
