﻿using FAMS.API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FAMS.API.DataAccess.Configurations
{
    public class ClassConfiguration : IEntityTypeConfiguration<Class>
    {
        public void Configure(EntityTypeBuilder<Class> builder)
        {
            builder
                .Property(e => e.CreatedDate)
                .HasDefaultValueSql("GETDATE()");

            builder
                .HasIndex(e => e.ClassName)
                .IsUnique();

            builder.Property(e => e.Status)
                .HasMaxLength(50)
                .HasDefaultValue("Active");

            builder.HasOne(d => d.CreatedUser).WithMany(p => p.CreatedClasses)
                .HasForeignKey(d => d.CreatedUserId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasOne(d => d.UpdatedUser).WithMany(p => p.UpdatedClasses)
                .HasForeignKey(d => d.UpdatedUserId);

            builder.HasOne(d => d.Program).WithMany(p => p.Classes)
                .HasForeignKey(d => d.ProgramId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder
                .HasData(
                    new Class
                    {
                        Id = 1,
                        ClassName = "NET_01",
                        CreatedUserId = 1,
                        Status = "Active",
                        ProgramId = 1
                    },
                    new Class
                    {
                        Id = 2,
                        ClassName = "React_01",
                        CreatedUserId = 1,
                        Status = "Active",
                        ProgramId = 2
                    },
                    new Class
                    {
                        Id = 3,
                        ClassName = "NET_02",
                        CreatedUserId = 6,
                        Status = "Active",
                        ProgramId = 1
                    },
                    new Class
                    {
                        Id = 4,
                        ClassName = "SAP_01",
                        CreatedUserId = 1,
                        Status = "Active",
                        ProgramId = 4
                    },
                    new Class
                    {
                        Id = 5,
                        ClassName = "Test_01",
                        CreatedUserId = 6,
                        Status = "Active",
                        ProgramId = 3
                    },
                    new Class
                    {
                        Id = 6,
                        ClassName = "React_02",
                        CreatedUserId = 1,
                        Status = "Active",
                        ProgramId = 2
                    }
                );
        }
    }
}
