﻿using FAMS.API.Models.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FAMS.API.Models
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(255)]
        public string? FullName { get; set; } //TODO: data type under consideration

        [Required]
        [StringLength(255)]
        public required string Email { get; set; }

        public DateTime? Dob { get; set; } //TODO: data type under consideration

        [StringLength(255)]
        public string? Address { get; set; } //TODO: data type under consideration

        [EnumDataType(typeof(Gender))]
        public required string Gender { get; set; }

        [MaxLength(20)]
        public string? Phone { get; set; } //TODO: data type under consideration

        [StringLength(255)]
        public string? Username { get; set; } //TODO: data type under consideration

        [Required]
        [StringLength(255)]
        public required string Password { get; set; }

        [EnumDataType(typeof(Role))]
        public required string Role { get; set; }

        //navigate
        public virtual ICollection<Class> CreatedClasses { get; set; } = new List<Class>();
        public virtual ICollection<Class> UpdatedClasses { get; set; } = new List<Class>();
        public virtual ICollection<Class> ManagedClasses { get; set; } = new List<Class>();
        public virtual ICollection<Program> CreatedPrograms { get; set; } = new List<Program>();
        public virtual ICollection<Program> UpdatedPrograms { get; set; } = new List<Program>();
        public virtual ICollection<Module> CreatedModules { get; set; } = new List<Module>();
        public virtual ICollection<Module> UpdatedModules { get; set; } = new List<Module>();
        public virtual ICollection<EmailTemplate> CreatedEmailTemplates { get; set; } = new List<EmailTemplate>();
        public virtual ICollection<EmailTemplate> UpdatedEmailTemplates { get; set; } = new List<EmailTemplate>();
        public virtual ICollection<EmailSend> EmailSends { get; set; } = new List<EmailSend>();
    }
}
