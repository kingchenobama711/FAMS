﻿using FAMS.API.Helper.Dtos.Query;
using FAMS.API.Helper.Dtos.Students;
using FAMS.API.Models;

namespace FAMS.API.DataAccess.Interfaces
{
    public interface IStudentRepository : IGenericRepository<Student>
    {
        Task<PagedResult<Student>> GetPagedStudents(QueryStudentsDto queryStudentsDto);
    }
}
