﻿namespace FAMS.API.Models.Enums;

public enum ReceiverType
{
    Remind,
    Inform
}