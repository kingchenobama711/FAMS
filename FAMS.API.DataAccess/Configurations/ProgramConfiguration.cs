﻿using FAMS.API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FAMS.API.DataAccess.Configurations
{
    public class ProgramConfiguration : IEntityTypeConfiguration<Program>
    {
        public void Configure(EntityTypeBuilder<Program> builder)
        {
            builder
                .Property(e => e.CreatedDate)
                .HasDefaultValueSql("GETDATE()");

            builder.Property(e => e.Status)
                .HasMaxLength(50)
                .HasDefaultValue("Active");

            builder.HasIndex(e => e.Code)
                .IsUnique();

            builder.HasOne(d => d.CreatedUser).WithMany(p => p.CreatedPrograms)
                .HasForeignKey(d => d.CreatedUserId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasOne(d => d.UpdatedUser).WithMany(p => p.UpdatedPrograms)
                .HasForeignKey(d => d.UpdatedUserId);

            builder
                .HasMany<Module>(s => s.Modules)
                .WithMany(c => c.Programs)
                .UsingEntity(
                    "ProgramModule",
                    l => l.HasOne(typeof(Module)).WithMany().HasForeignKey("ModuleId").HasPrincipalKey(nameof(Module.Id)),
                    r => r.HasOne(typeof(Program)).WithMany().HasForeignKey("ProgramId").HasPrincipalKey(nameof(Program.Id)),
                    j => j.HasKey("ModuleId", "ProgramId"));

            builder
                .HasData(
                    new Program
                    {
                        Id = 1,
                        ProgramName = "ASP .NET Core for FA student",
                        Code = "NET",
                        CreatedUserId = 1,
                        Status = "Active"
                    },
                    new Program
                    {
                        Id = 2,
                        ProgramName = "React for FA student",
                        Code = "React",
                        CreatedUserId = 6,
                        Status = "Active"
                    },
                    new Program
                    {
                        Id = 3,
                        ProgramName = "Test for FA student",
                        Code = "Test",
                        CreatedUserId = 1,
                        Status = "Active"
                    },
                    new Program
                    {
                        Id = 4,
                        ProgramName = "SAP for FA student",
                        Code = "SAP",
                        CreatedUserId = 6,
                        Status = "Active"
                    }
                );
        }
    }
}
