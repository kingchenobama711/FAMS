﻿using AutoMapper;
using FAMS.API.BusinessLogic.Interfaces;
using FAMS.API.Helper.Dtos.Query;
using FAMS.API.Helper.Dtos.Students;
using Microsoft.AspNetCore.Mvc;

namespace FAMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        private readonly IStudentService _studentService;
        private readonly IMapper _mapper;

        public StudentsController(IStudentService studentService, IMapper mapper)
        {
            _studentService = studentService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<PagedResult<GetStudentDto>>> GetStudents([FromQuery] int pageSize = 10, [FromQuery] int pageNumber = 1, [FromQuery] string searchQuery = "")
        {
            var result = await _studentService.GetStudents(new QueryStudentsDto()
            {
                PageNumber = pageNumber,
                PageSize = pageSize,
                SearchQuery = searchQuery
            });

            return _mapper.Map<PagedResult<GetStudentDto>>(result);
        }
    }
}