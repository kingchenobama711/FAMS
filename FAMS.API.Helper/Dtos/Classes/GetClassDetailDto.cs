﻿using FAMS.API.Helper.Dtos.Program;
using FAMS.API.Helper.Dtos.StudentClass;
using FAMS.API.Helper.Dtos.Users;

namespace FAMS.API.Helper.Dtos.Classes
{
    public class GetClassDetailDto
    {
        public int Id { get; set; }

        public required string ClassName { get; set; }

        public DateTime? StartDate { get; set; } //TODO: data type under consideration

        public DateTime? EndDate { get; set; } //TODO: data type under consideration

        public DateTime CreatedDate { get; set; }

        public int CreatedUserId { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUserId { get; set; }

        public string? Duration { get; set; } //TODO: data type under consideration

        public string? Location { get; set; } //TODO: data type under consideration

        public required string Status { get; set; }

        public int ProgramId { get; set; }

        public virtual GetProgramDto Program { get; set; } = null!;

        public virtual GetUserDto CreatedUser { get; set; } = null!;

        public virtual GetUserDto? UpdatedUser { get; set; }

        public virtual ICollection<GetUserDto> Managers { get; set; } = new List<GetUserDto>();

        public virtual ICollection<GetStudentClassDto> Students { get; set; } = new List<GetStudentClassDto>();
    }
}
