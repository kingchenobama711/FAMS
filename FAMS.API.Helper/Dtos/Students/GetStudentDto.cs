﻿namespace FAMS.API.Helper.Dtos.Students
{
    public class GetStudentDto
    {
        public int Id { get; set; }

        public required string FullName { get; set; }

        public DateTime? Dob { get; set; } //TODO: data type under consideration

        public required string Gender { get; set; }

        public string? Phone { get; set; } //TODO: data type under consideration

        public required string Email { get; set; }

        public string? School { get; set; }

        public required string Major { get; set; }  //TODO: data type under consideration

        public DateTime? GraduatedDate { get; set; }

        public double? Gpa { get; set; } //TODO: data type under consideration

        public string? Address { get; set; } //TODO: data type under consideration

        public string? FaAccount { get; set; } //TODO: data type under consideration

        public string? Type { get; set; } //TODO: unknown prop ???

        public required string Status { get; set; } //TODO: data type under consideration

        public string? ReCer { get; set; } //TODO: unknown prop ???

        public DateTime JoinedDate { get; set; } //TODO: data type under consideration

        public required string Area { get; set; } //TODO: data type under consideration

    }
}
