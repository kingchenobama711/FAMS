﻿using FAMS.API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FAMS.API.DataAccess.Configurations
{
    public class EmailTemplateConfiguration : IEntityTypeConfiguration<EmailTemplate>
    {
        public void Configure(EntityTypeBuilder<EmailTemplate> builder)
        {
            builder.HasOne(d => d.CreatedUser).WithMany(p => p.CreatedEmailTemplates)
                .HasForeignKey(d => d.CreatedUserId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasOne(d => d.UpdatedUser).WithMany(p => p.UpdatedEmailTemplates)
                .HasForeignKey(d => d.UpdatedUserId);
            builder.HasData(
            new EmailTemplate
            {
                Id = 1,
                Name = "Reservation Notification",
                Description = "Chào [Tên Học Viên],\n\n"
+ "Chúc bạn một ngày tốt lành! Tôi là [Tên Admin], Admin của Fresher Academy.\n"
+ "Chúng tôi rất vui vì bạn đã quyết định bảo lưu tại Fresher Academy để giữ vững kiến thức và kỹ năng của mình. Hiện tại, tôi muốn nhắc nhở bạn rằng thời hạn bảo lưu của bạn sẽ kết thúc trong vòng một tháng nữa.\n"
+ "Để giúp bạn dễ dàng quay lại và tiếp tục hành trình học tập, chúng tôi đề xuất bạn nên liên hệ với chúng tôi trước khi thời hạn bảo lưu hết hạn. Bằng cách này, chúng tôi có thể hỗ trợ bạn trong quá trình xếp lớp và đảm bảo rằng bạn sẽ có trải nghiệm học tập tốt nhất tại Fresher Academy.\n"
+ "Hãy liên hệ với chúng tôi qua số điện thoại: [Số Điện Thoại của Bạn]. Chúng tôi sẽ sẵn lòng hỗ trợ bạn với mọi thắc mắc và yêu cầu của bạn.\n"
+ "Chân thành cảm ơn sự quan tâm và cam kết của bạn đối với chương trình học tại Fresher Academy. Chúng tôi mong sớm được gặp lại bạn và chia sẻ những kiến thức mới.\n"
+ "Trân trọng,\n"
+ "[Tên Của Admin]\n"
+ "Admin Fresher Academy",
                CreatedDate = DateTime.Today,
                CreatedUserId = 1,
                UpdatedDate = DateTime.Today,
                UpdatedUserId = 1
            },

            new EmailTemplate
            {
                Id = 2,
                Name = "Grades Notification",
                Description = "Chào [Tên Học Viên],\n\n"
+ "Chúc bạn một ngày tốt lành! Tôi là [Tên Admin], Admin của Fresher Academy.\n"
+ "Tôi trân trọng thông báo kết quả học tập kỳ [Mã kỳ học] của sinh viên [Tên sinh viên]"
+ "Hãy liên hệ với chúng tôi qua số điện thoại: [Số Điện Thoại của Bạn]. Chúng tôi sẽ sẵn lòng hỗ trợ bạn với mọi thắc mắc và yêu cầu của bạn.\n"
+ "Chân thành cảm ơn sự quan tâm và cam kết của bạn đối với chương trình học tại Fresher Academy. Chúng tôi mong sớm được gặp lại bạn và chia sẻ những kiến thức mới.\n"
+ "Trân trọng,\n"
+ "[Tên Của Admin]\n"
+ "Admin Fresher Academy",
                CreatedDate = DateTime.Today,
                CreatedUserId = 2,
                UpdatedDate = DateTime.Today,
                UpdatedUserId = 2
            },
            new EmailTemplate
            {
                Id = 3,
                Name = "Asking permission for absenting in class",
                Description = "Kính gửi anh/chị [tên admin], em tên là [tên sinh viên]\n"
                + "\nEm viết đơn này mong được xin nghỉ lớp [Mã/Tên lớp] vào lúc [thời gian muốn nghỉ]\n"
                + "\nvì lý do [Lý do nghỉ].",
                CreatedDate = DateTime.Today,
                CreatedUserId = 3,
                UpdatedDate = DateTime.Today,
                UpdatedUserId = 3
            }
        );
        }
    }
}
