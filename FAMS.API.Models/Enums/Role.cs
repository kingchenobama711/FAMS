﻿namespace FAMS.API.Models.Enums;

public enum Role
{
    SuperAdmin,
    ClassAdmin,
    Trainer
}