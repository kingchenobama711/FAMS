﻿namespace FAMS.API.Helper.Dtos.Auth
{
    public class AuthResponseDto
    {
        public required string Token { get; set; }
    }
}
