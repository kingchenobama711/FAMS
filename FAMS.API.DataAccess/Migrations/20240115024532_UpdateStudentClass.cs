﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FAMS.API.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class UpdateStudentClass : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AttendingStatus",
                table: "StudentClass",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<DateTime>(
                name: "CertificationDate",
                table: "StudentClass",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CertificationStatus",
                table: "StudentClass",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<double>(
                name: "FinalScore",
                table: "StudentClass",
                type: "float",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GpaLevel",
                table: "StudentClass",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Method",
                table: "StudentClass",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Result",
                table: "StudentClass",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AttendingStatus",
                table: "StudentClass");

            migrationBuilder.DropColumn(
                name: "CertificationDate",
                table: "StudentClass");

            migrationBuilder.DropColumn(
                name: "CertificationStatus",
                table: "StudentClass");

            migrationBuilder.DropColumn(
                name: "FinalScore",
                table: "StudentClass");

            migrationBuilder.DropColumn(
                name: "GpaLevel",
                table: "StudentClass");

            migrationBuilder.DropColumn(
                name: "Method",
                table: "StudentClass");

            migrationBuilder.DropColumn(
                name: "Result",
                table: "StudentClass");
        }
    }
}
