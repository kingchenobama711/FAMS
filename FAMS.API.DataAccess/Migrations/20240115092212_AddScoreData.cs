﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace FAMS.API.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class AddScoreData : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Assignment_Modules_ModuleId",
                table: "Assignment");

            migrationBuilder.DropForeignKey(
                name: "FK_Assignment_Users_CreatedUserId",
                table: "Assignment");

            migrationBuilder.DropForeignKey(
                name: "FK_Assignment_Users_UpdatedUserId",
                table: "Assignment");

            migrationBuilder.DropForeignKey(
                name: "FK_Score_Assignment_AssignmentId",
                table: "Score");

            migrationBuilder.DropForeignKey(
                name: "FK_Score_Students_StudentId",
                table: "Score");

            migrationBuilder.DropForeignKey(
                name: "FK_StudentModule_Modules_ModuleId",
                table: "StudentModule");

            migrationBuilder.DropForeignKey(
                name: "FK_StudentModule_Students_StudentId",
                table: "StudentModule");

            migrationBuilder.DropPrimaryKey(
                name: "PK_StudentModule",
                table: "StudentModule");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Score",
                table: "Score");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Assignment",
                table: "Assignment");

            migrationBuilder.RenameTable(
                name: "StudentModule",
                newName: "StudentModules");

            migrationBuilder.RenameTable(
                name: "Score",
                newName: "Scores");

            migrationBuilder.RenameTable(
                name: "Assignment",
                newName: "Assignments");

            migrationBuilder.RenameIndex(
                name: "IX_StudentModule_StudentId",
                table: "StudentModules",
                newName: "IX_StudentModules_StudentId");

            migrationBuilder.RenameIndex(
                name: "IX_StudentModule_ModuleId",
                table: "StudentModules",
                newName: "IX_StudentModules_ModuleId");

            migrationBuilder.RenameIndex(
                name: "IX_Score_StudentId",
                table: "Scores",
                newName: "IX_Scores_StudentId");

            migrationBuilder.RenameIndex(
                name: "IX_Score_AssignmentId",
                table: "Scores",
                newName: "IX_Scores_AssignmentId");

            migrationBuilder.RenameIndex(
                name: "IX_Assignment_UpdatedUserId",
                table: "Assignments",
                newName: "IX_Assignments_UpdatedUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Assignment_ModuleId",
                table: "Assignments",
                newName: "IX_Assignments_ModuleId");

            migrationBuilder.RenameIndex(
                name: "IX_Assignment_CreatedUserId",
                table: "Assignments",
                newName: "IX_Assignments_CreatedUserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_StudentModules",
                table: "StudentModules",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Scores",
                table: "Scores",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Assignments",
                table: "Assignments",
                column: "Id");

            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "Id", "Address", "Area", "Dob", "Email", "FaAccount", "FullName", "Gender", "Gpa", "GraduatedDate", "JoinedDate", "Major", "Phone", "ReCer", "School", "Status", "Type" },
                values: new object[,]
                {
                    { 1, null, "HoChiMinh", null, "student1@gmail.com", null, "Sinh viên 1", "Male", 0.0, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Net", null, null, null, "Active", null },
                    { 2, null, "QuyNhon", null, "student2@gmail.com", null, "Sinh viên 2", "Male", 0.0, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "React", null, null, null, "Active", null },
                    { 3, null, "DaNang", null, "student3@gmail.com", null, "Sinh viên 3", "Female", 0.0, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Test", null, null, null, "Active", null },
                    { 4, null, "HaNoi", null, "student4@gmail.com", null, "Sinh viên 4", "Male", 0.0, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sap", null, null, null, "Active", null }
                });

            migrationBuilder.InsertData(
                table: "Scores",
                columns: new[] { "Id", "AssignmentId", "ScoreValue", "StudentId", "SubmissionDate" },
                values: new object[] { 1, 1, 5.0, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "StudentClasses",
                columns: new[] { "Id", "AttendingStatus", "CertificationDate", "CertificationStatus", "ClassId", "FinalScore", "GpaLevel", "Method", "Result", "StudentId" },
                values: new object[,]
                {
                    { 1, "InClass", null, "NotYet", 1, null, null, null, "NotYet", 1 },
                    { 2, "InClass", null, "NotYet", 2, null, null, null, "NotYet", 2 },
                    { 3, "InClass", null, "NotYet", 5, null, null, null, "NotYet", 3 },
                    { 4, "InClass", null, "NotYet", 4, null, null, null, "NotYet", 4 }
                });

            migrationBuilder.InsertData(
                table: "StudentModules",
                columns: new[] { "Id", "ModuleId", "ModuleLevel", "ModuleScore", "StudentId" },
                values: new object[,]
                {
                    { 1, 1, "Unknown", 7.0, 1 },
                    { 2, 4, "Unknown", 9.0, 1 },
                    { 3, 2, "Unknown", 7.5, 2 },
                    { 4, 3, "Unknown", 6.0, 2 },
                    { 5, 8, "Unknown", 6.0, 3 },
                    { 6, 5, "Unknown", 4.0, 4 }
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Assignments_Modules_ModuleId",
                table: "Assignments",
                column: "ModuleId",
                principalTable: "Modules",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Assignments_Users_CreatedUserId",
                table: "Assignments",
                column: "CreatedUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Assignments_Users_UpdatedUserId",
                table: "Assignments",
                column: "UpdatedUserId",
                principalTable: "Users",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Scores_Assignments_AssignmentId",
                table: "Scores",
                column: "AssignmentId",
                principalTable: "Assignments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Scores_Students_StudentId",
                table: "Scores",
                column: "StudentId",
                principalTable: "Students",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StudentModules_Modules_ModuleId",
                table: "StudentModules",
                column: "ModuleId",
                principalTable: "Modules",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StudentModules_Students_StudentId",
                table: "StudentModules",
                column: "StudentId",
                principalTable: "Students",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Assignments_Modules_ModuleId",
                table: "Assignments");

            migrationBuilder.DropForeignKey(
                name: "FK_Assignments_Users_CreatedUserId",
                table: "Assignments");

            migrationBuilder.DropForeignKey(
                name: "FK_Assignments_Users_UpdatedUserId",
                table: "Assignments");

            migrationBuilder.DropForeignKey(
                name: "FK_Scores_Assignments_AssignmentId",
                table: "Scores");

            migrationBuilder.DropForeignKey(
                name: "FK_Scores_Students_StudentId",
                table: "Scores");

            migrationBuilder.DropForeignKey(
                name: "FK_StudentModules_Modules_ModuleId",
                table: "StudentModules");

            migrationBuilder.DropForeignKey(
                name: "FK_StudentModules_Students_StudentId",
                table: "StudentModules");

            migrationBuilder.DropPrimaryKey(
                name: "PK_StudentModules",
                table: "StudentModules");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Scores",
                table: "Scores");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Assignments",
                table: "Assignments");

            migrationBuilder.DeleteData(
                table: "Scores",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "StudentClasses",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "StudentClasses",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "StudentClasses",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "StudentClasses",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "StudentModules",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "StudentModules",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "StudentModules",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "StudentModules",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "StudentModules",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "StudentModules",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.RenameTable(
                name: "StudentModules",
                newName: "StudentModule");

            migrationBuilder.RenameTable(
                name: "Scores",
                newName: "Score");

            migrationBuilder.RenameTable(
                name: "Assignments",
                newName: "Assignment");

            migrationBuilder.RenameIndex(
                name: "IX_StudentModules_StudentId",
                table: "StudentModule",
                newName: "IX_StudentModule_StudentId");

            migrationBuilder.RenameIndex(
                name: "IX_StudentModules_ModuleId",
                table: "StudentModule",
                newName: "IX_StudentModule_ModuleId");

            migrationBuilder.RenameIndex(
                name: "IX_Scores_StudentId",
                table: "Score",
                newName: "IX_Score_StudentId");

            migrationBuilder.RenameIndex(
                name: "IX_Scores_AssignmentId",
                table: "Score",
                newName: "IX_Score_AssignmentId");

            migrationBuilder.RenameIndex(
                name: "IX_Assignments_UpdatedUserId",
                table: "Assignment",
                newName: "IX_Assignment_UpdatedUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Assignments_ModuleId",
                table: "Assignment",
                newName: "IX_Assignment_ModuleId");

            migrationBuilder.RenameIndex(
                name: "IX_Assignments_CreatedUserId",
                table: "Assignment",
                newName: "IX_Assignment_CreatedUserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_StudentModule",
                table: "StudentModule",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Score",
                table: "Score",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Assignment",
                table: "Assignment",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Assignment_Modules_ModuleId",
                table: "Assignment",
                column: "ModuleId",
                principalTable: "Modules",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Assignment_Users_CreatedUserId",
                table: "Assignment",
                column: "CreatedUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Assignment_Users_UpdatedUserId",
                table: "Assignment",
                column: "UpdatedUserId",
                principalTable: "Users",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Score_Assignment_AssignmentId",
                table: "Score",
                column: "AssignmentId",
                principalTable: "Assignment",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Score_Students_StudentId",
                table: "Score",
                column: "StudentId",
                principalTable: "Students",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StudentModule_Modules_ModuleId",
                table: "StudentModule",
                column: "ModuleId",
                principalTable: "Modules",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StudentModule_Students_StudentId",
                table: "StudentModule",
                column: "StudentId",
                principalTable: "Students",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
