﻿namespace FAMS.API.Helper.Dtos.StudentClass
{
    public class GetStudentClassDto
    {
        public int Id { get; set; }

        public int StudentId { get; set; }

        public int ClassId { get; set; }

        public required string AttendingStatus { get; set; }

        public required string Result { get; set; }

        public double? FinalScore { get; set; } //TODO: type considering

        public string? GpaLevel { get; set; } //TODO: type considering

        public required string CertificationStatus { get; set; }

        public DateTime? CertificationDate { get; set; }

        public string? Method { get; set; } //TODO: unknown prop
    }
}
