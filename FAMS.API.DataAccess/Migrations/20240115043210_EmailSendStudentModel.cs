﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FAMS.API.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class EmailSendStudentModel : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Classes_Program_ProgramId",
                table: "Classes");

            migrationBuilder.DropForeignKey(
                name: "FK_EmailSend_EmailTemplate_EmailTemplateId",
                table: "EmailSend");

            migrationBuilder.DropForeignKey(
                name: "FK_EmailSend_Users_SenderId",
                table: "EmailSend");

            migrationBuilder.DropForeignKey(
                name: "FK_EmailTemplate_Users_CreatedUserId",
                table: "EmailTemplate");

            migrationBuilder.DropForeignKey(
                name: "FK_EmailTemplate_Users_UpdatedUserId",
                table: "EmailTemplate");

            migrationBuilder.DropForeignKey(
                name: "FK_Module_Users_CreatedUserId",
                table: "Module");

            migrationBuilder.DropForeignKey(
                name: "FK_Module_Users_UpdatedUserId",
                table: "Module");

            migrationBuilder.DropForeignKey(
                name: "FK_Program_Users_CreatedUserId",
                table: "Program");

            migrationBuilder.DropForeignKey(
                name: "FK_Program_Users_UpdatedUserId",
                table: "Program");

            migrationBuilder.DropForeignKey(
                name: "FK_ProgramModule_Module_ModuleId",
                table: "ProgramModule");

            migrationBuilder.DropForeignKey(
                name: "FK_ProgramModule_Program_ProgramId",
                table: "ProgramModule");

            migrationBuilder.DropForeignKey(
                name: "FK_StudentClass_Classes_ClassId",
                table: "StudentClass");

            migrationBuilder.DropForeignKey(
                name: "FK_StudentClass_Student_StudentId",
                table: "StudentClass");

            migrationBuilder.DropPrimaryKey(
                name: "PK_StudentClass",
                table: "StudentClass");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Student",
                table: "Student");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Program",
                table: "Program");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Module",
                table: "Module");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EmailTemplate",
                table: "EmailTemplate");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EmailSend",
                table: "EmailSend");

            migrationBuilder.RenameTable(
                name: "StudentClass",
                newName: "StudentClasses");

            migrationBuilder.RenameTable(
                name: "Student",
                newName: "Students");

            migrationBuilder.RenameTable(
                name: "Program",
                newName: "Programs");

            migrationBuilder.RenameTable(
                name: "Module",
                newName: "Modules");

            migrationBuilder.RenameTable(
                name: "EmailTemplate",
                newName: "EmailTemplates");

            migrationBuilder.RenameTable(
                name: "EmailSend",
                newName: "EmailSends");

            migrationBuilder.RenameIndex(
                name: "IX_StudentClass_StudentId",
                table: "StudentClasses",
                newName: "IX_StudentClasses_StudentId");

            migrationBuilder.RenameIndex(
                name: "IX_StudentClass_ClassId",
                table: "StudentClasses",
                newName: "IX_StudentClasses_ClassId");

            migrationBuilder.RenameIndex(
                name: "IX_Program_UpdatedUserId",
                table: "Programs",
                newName: "IX_Programs_UpdatedUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Program_CreatedUserId",
                table: "Programs",
                newName: "IX_Programs_CreatedUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Program_Code",
                table: "Programs",
                newName: "IX_Programs_Code");

            migrationBuilder.RenameIndex(
                name: "IX_Module_UpdatedUserId",
                table: "Modules",
                newName: "IX_Modules_UpdatedUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Module_ModuleName",
                table: "Modules",
                newName: "IX_Modules_ModuleName");

            migrationBuilder.RenameIndex(
                name: "IX_Module_CreatedUserId",
                table: "Modules",
                newName: "IX_Modules_CreatedUserId");

            migrationBuilder.RenameIndex(
                name: "IX_EmailTemplate_UpdatedUserId",
                table: "EmailTemplates",
                newName: "IX_EmailTemplates_UpdatedUserId");

            migrationBuilder.RenameIndex(
                name: "IX_EmailTemplate_CreatedUserId",
                table: "EmailTemplates",
                newName: "IX_EmailTemplates_CreatedUserId");

            migrationBuilder.RenameIndex(
                name: "IX_EmailSend_SenderId",
                table: "EmailSends",
                newName: "IX_EmailSends_SenderId");

            migrationBuilder.RenameIndex(
                name: "IX_EmailSend_EmailTemplateId",
                table: "EmailSends",
                newName: "IX_EmailSends_EmailTemplateId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_StudentClasses",
                table: "StudentClasses",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Students",
                table: "Students",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Programs",
                table: "Programs",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Modules",
                table: "Modules",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EmailTemplates",
                table: "EmailTemplates",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EmailSends",
                table: "EmailSends",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "EmailSendStudent",
                columns: table => new
                {
                    StudentId = table.Column<int>(type: "int", nullable: false),
                    EmailSendId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailSendStudent", x => new { x.StudentId, x.EmailSendId });
                    table.ForeignKey(
                        name: "FK_EmailSendStudent_EmailSends_EmailSendId",
                        column: x => x.EmailSendId,
                        principalTable: "EmailSends",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EmailSendStudent_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EmailSendStudent_EmailSendId",
                table: "EmailSendStudent",
                column: "EmailSendId");

            migrationBuilder.AddForeignKey(
                name: "FK_Classes_Programs_ProgramId",
                table: "Classes",
                column: "ProgramId",
                principalTable: "Programs",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_EmailSends_EmailTemplates_EmailTemplateId",
                table: "EmailSends",
                column: "EmailTemplateId",
                principalTable: "EmailTemplates",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_EmailSends_Users_SenderId",
                table: "EmailSends",
                column: "SenderId",
                principalTable: "Users",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_EmailTemplates_Users_CreatedUserId",
                table: "EmailTemplates",
                column: "CreatedUserId",
                principalTable: "Users",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_EmailTemplates_Users_UpdatedUserId",
                table: "EmailTemplates",
                column: "UpdatedUserId",
                principalTable: "Users",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Modules_Users_CreatedUserId",
                table: "Modules",
                column: "CreatedUserId",
                principalTable: "Users",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Modules_Users_UpdatedUserId",
                table: "Modules",
                column: "UpdatedUserId",
                principalTable: "Users",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProgramModule_Modules_ModuleId",
                table: "ProgramModule",
                column: "ModuleId",
                principalTable: "Modules",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProgramModule_Programs_ProgramId",
                table: "ProgramModule",
                column: "ProgramId",
                principalTable: "Programs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Programs_Users_CreatedUserId",
                table: "Programs",
                column: "CreatedUserId",
                principalTable: "Users",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Programs_Users_UpdatedUserId",
                table: "Programs",
                column: "UpdatedUserId",
                principalTable: "Users",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_StudentClasses_Classes_ClassId",
                table: "StudentClasses",
                column: "ClassId",
                principalTable: "Classes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StudentClasses_Students_StudentId",
                table: "StudentClasses",
                column: "StudentId",
                principalTable: "Students",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Classes_Programs_ProgramId",
                table: "Classes");

            migrationBuilder.DropForeignKey(
                name: "FK_EmailSends_EmailTemplates_EmailTemplateId",
                table: "EmailSends");

            migrationBuilder.DropForeignKey(
                name: "FK_EmailSends_Users_SenderId",
                table: "EmailSends");

            migrationBuilder.DropForeignKey(
                name: "FK_EmailTemplates_Users_CreatedUserId",
                table: "EmailTemplates");

            migrationBuilder.DropForeignKey(
                name: "FK_EmailTemplates_Users_UpdatedUserId",
                table: "EmailTemplates");

            migrationBuilder.DropForeignKey(
                name: "FK_Modules_Users_CreatedUserId",
                table: "Modules");

            migrationBuilder.DropForeignKey(
                name: "FK_Modules_Users_UpdatedUserId",
                table: "Modules");

            migrationBuilder.DropForeignKey(
                name: "FK_ProgramModule_Modules_ModuleId",
                table: "ProgramModule");

            migrationBuilder.DropForeignKey(
                name: "FK_ProgramModule_Programs_ProgramId",
                table: "ProgramModule");

            migrationBuilder.DropForeignKey(
                name: "FK_Programs_Users_CreatedUserId",
                table: "Programs");

            migrationBuilder.DropForeignKey(
                name: "FK_Programs_Users_UpdatedUserId",
                table: "Programs");

            migrationBuilder.DropForeignKey(
                name: "FK_StudentClasses_Classes_ClassId",
                table: "StudentClasses");

            migrationBuilder.DropForeignKey(
                name: "FK_StudentClasses_Students_StudentId",
                table: "StudentClasses");

            migrationBuilder.DropTable(
                name: "EmailSendStudent");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Students",
                table: "Students");

            migrationBuilder.DropPrimaryKey(
                name: "PK_StudentClasses",
                table: "StudentClasses");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Programs",
                table: "Programs");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Modules",
                table: "Modules");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EmailTemplates",
                table: "EmailTemplates");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EmailSends",
                table: "EmailSends");

            migrationBuilder.RenameTable(
                name: "Students",
                newName: "Student");

            migrationBuilder.RenameTable(
                name: "StudentClasses",
                newName: "StudentClass");

            migrationBuilder.RenameTable(
                name: "Programs",
                newName: "Program");

            migrationBuilder.RenameTable(
                name: "Modules",
                newName: "Module");

            migrationBuilder.RenameTable(
                name: "EmailTemplates",
                newName: "EmailTemplate");

            migrationBuilder.RenameTable(
                name: "EmailSends",
                newName: "EmailSend");

            migrationBuilder.RenameIndex(
                name: "IX_StudentClasses_StudentId",
                table: "StudentClass",
                newName: "IX_StudentClass_StudentId");

            migrationBuilder.RenameIndex(
                name: "IX_StudentClasses_ClassId",
                table: "StudentClass",
                newName: "IX_StudentClass_ClassId");

            migrationBuilder.RenameIndex(
                name: "IX_Programs_UpdatedUserId",
                table: "Program",
                newName: "IX_Program_UpdatedUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Programs_CreatedUserId",
                table: "Program",
                newName: "IX_Program_CreatedUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Programs_Code",
                table: "Program",
                newName: "IX_Program_Code");

            migrationBuilder.RenameIndex(
                name: "IX_Modules_UpdatedUserId",
                table: "Module",
                newName: "IX_Module_UpdatedUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Modules_ModuleName",
                table: "Module",
                newName: "IX_Module_ModuleName");

            migrationBuilder.RenameIndex(
                name: "IX_Modules_CreatedUserId",
                table: "Module",
                newName: "IX_Module_CreatedUserId");

            migrationBuilder.RenameIndex(
                name: "IX_EmailTemplates_UpdatedUserId",
                table: "EmailTemplate",
                newName: "IX_EmailTemplate_UpdatedUserId");

            migrationBuilder.RenameIndex(
                name: "IX_EmailTemplates_CreatedUserId",
                table: "EmailTemplate",
                newName: "IX_EmailTemplate_CreatedUserId");

            migrationBuilder.RenameIndex(
                name: "IX_EmailSends_SenderId",
                table: "EmailSend",
                newName: "IX_EmailSend_SenderId");

            migrationBuilder.RenameIndex(
                name: "IX_EmailSends_EmailTemplateId",
                table: "EmailSend",
                newName: "IX_EmailSend_EmailTemplateId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Student",
                table: "Student",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_StudentClass",
                table: "StudentClass",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Program",
                table: "Program",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Module",
                table: "Module",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EmailTemplate",
                table: "EmailTemplate",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EmailSend",
                table: "EmailSend",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Classes_Program_ProgramId",
                table: "Classes",
                column: "ProgramId",
                principalTable: "Program",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_EmailSend_EmailTemplate_EmailTemplateId",
                table: "EmailSend",
                column: "EmailTemplateId",
                principalTable: "EmailTemplate",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_EmailSend_Users_SenderId",
                table: "EmailSend",
                column: "SenderId",
                principalTable: "Users",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_EmailTemplate_Users_CreatedUserId",
                table: "EmailTemplate",
                column: "CreatedUserId",
                principalTable: "Users",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_EmailTemplate_Users_UpdatedUserId",
                table: "EmailTemplate",
                column: "UpdatedUserId",
                principalTable: "Users",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Module_Users_CreatedUserId",
                table: "Module",
                column: "CreatedUserId",
                principalTable: "Users",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Module_Users_UpdatedUserId",
                table: "Module",
                column: "UpdatedUserId",
                principalTable: "Users",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Program_Users_CreatedUserId",
                table: "Program",
                column: "CreatedUserId",
                principalTable: "Users",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Program_Users_UpdatedUserId",
                table: "Program",
                column: "UpdatedUserId",
                principalTable: "Users",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProgramModule_Module_ModuleId",
                table: "ProgramModule",
                column: "ModuleId",
                principalTable: "Module",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProgramModule_Program_ProgramId",
                table: "ProgramModule",
                column: "ProgramId",
                principalTable: "Program",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StudentClass_Classes_ClassId",
                table: "StudentClass",
                column: "ClassId",
                principalTable: "Classes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StudentClass_Student_StudentId",
                table: "StudentClass",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
