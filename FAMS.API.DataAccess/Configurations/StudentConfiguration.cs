﻿using FAMS.API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FAMS.API.DataAccess.Configurations;

public class StudentConfiguration : IEntityTypeConfiguration<Student>
{
    public void Configure(EntityTypeBuilder<Student> builder)
    {
        builder.HasData(
            new Student
            {
                JoinedDate = DateTime.Now,
                Id = 1,
                Gender = "Male",
                Major = "Net",
                Email = "student1@gmail.com",
                Status = "Active",
                Area = "HoChiMinh",
                FullName = "Sinh viên 1"
            },
            new Student
            {
                JoinedDate = DateTime.Now,
                Id = 2,
                Gender = "Male",
                Major = "React",
                Email = "student2@gmail.com",
                Status = "Active",
                Area = "QuyNhon",
                FullName = "Sinh viên 2"
            },
            new Student
            {
                JoinedDate = DateTime.Now,
                Id = 3,
                Gender = "Female",
                Major = "Test",
                Email = "student3@gmail.com",
                Status = "Active",
                Area = "DaNang",
                FullName = "Sinh viên 3"
            },
            new Student
            {
                JoinedDate = DateTime.Now,
                Id = 4,
                Gender = "Male",
                Major = "Sap",
                Email = "student4@gmail.com",
                Status = "Active",
                Area = "HaNoi",
                FullName = "Sinh viên 4"
            }
            );
    }
}