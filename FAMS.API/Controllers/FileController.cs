﻿using Microsoft.AspNetCore.Mvc;

namespace FAMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {

        [HttpGet]
        [Route("student-template")]
        public IActionResult DownloadExcel()
        {
            string filePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "student-template.xlsx");
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "StudentTemplate.xlsx");
        }
    }
}
