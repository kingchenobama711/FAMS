﻿using FAMS.API.Models.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FAMS.API.Models
{
    public class StudentClass
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int StudentId { get; set; }

        public int ClassId { get; set; }

        [EnumDataType(typeof(AttendingStatus))]
        public required string AttendingStatus { get; set; }

        [EnumDataType(typeof(Result))]
        public required string Result { get; set; }

        public double? FinalScore { get; set; } //TODO: type considering

        [EnumDataType(typeof(GpaLevel))]
        public string? GpaLevel { get; set; } //TODO: type considering

        [EnumDataType(typeof(CertificationStatus))]
        public required string CertificationStatus { get; set; }

        public DateTime? CertificationDate { get; set; }

        public string? Method { get; set; } //TODO: unknown prop

        //navigator
        public virtual Student Student { get; set; } = null!;
        public virtual Class Class { get; set; } = null!;
    }
}
