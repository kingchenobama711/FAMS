﻿using AutoMapper;
using FAMS.API.BusinessLogic.Interfaces;
using FAMS.API.Helper.Dtos.Classes;
using Microsoft.AspNetCore.Mvc;

namespace FAMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClassesController : ControllerBase
    {
        private readonly IClassService _classService;
        private readonly IMapper _mapper;

        public ClassesController(IClassService classService, IMapper mapper)
        {
            _classService = classService;
            _mapper = mapper;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<GetClassDetailDto>> GetClassDetail(int id)
        {
            var result = await _classService.GetClassDetailById(id);
            return _mapper.Map<GetClassDetailDto>(result);
        }
    }
}
