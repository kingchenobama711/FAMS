﻿using FAMS.API.Models.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FAMS.API.Models
{
    public class Class
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public required string ClassName { get; set; }

        public DateTime? StartDate { get; set; } //TODO: data type under consideration

        public DateTime? EndDate { get; set; } //TODO: data type under consideration

        public DateTime CreatedDate { get; set; }

        public int CreatedUserId { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUserId { get; set; }

        [StringLength(255)]
        public string? Duration { get; set; } //TODO: data type under consideration

        [StringLength(255)]
        public string? Location { get; set; } //TODO: data type under consideration

        [EnumDataType(typeof(ClassStatus))]
        public string Status { get; set; } = "Active";

        public int ProgramId { get; set; }

        //navigate
        public virtual Program Program { get; set; } = null!;
        public virtual User CreatedUser { get; set; } = null!;
        public virtual User? UpdatedUser { get; set; }
        public virtual ICollection<User> Managers { get; set; } = new List<User>();
        public virtual ICollection<StudentClass> Students { get; set; } = new List<StudentClass>();
    }
}
