﻿using FAMS.API.Models.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FAMS.API.Models
{
    public class Student
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(255)]
        public required string FullName { get; set; }

        public DateTime? Dob { get; set; } //TODO: data type under consideration

        [EnumDataType(typeof(Gender))]
        public required string Gender { get; set; }

        [MaxLength(20)]
        public string? Phone { get; set; } //TODO: data type under consideration

        [Required]
        public required string Email { get; set; }

        [StringLength(255)]
        public string? School { get; set; }

        [EnumDataType(typeof(Major))]
        public required string Major { get; set; }  //TODO: data type under consideration

        public DateTime? GraduatedDate { get; set; }

        public double? Gpa { get; set; } //TODO: data type under consideration

        [StringLength(255)]
        public string? Address { get; set; } //TODO: data type under consideration

        [StringLength(255)]
        public string? FaAccount { get; set; } //TODO: data type under consideration

        [StringLength(255)]
        public string? Type { get; set; } //TODO: unknown prop ???

        [EnumDataType(typeof(StudentStatus))]
        public required string Status { get; set; } //TODO: data type under consideration

        [StringLength(255)]
        public string? ReCer { get; set; } //TODO: unknown prop ???

        public DateTime JoinedDate { get; set; } //TODO: data type under consideration

        [EnumDataType(typeof(Area))]
        public required string Area { get; set; } //TODO: data type under consideration

        //navigator
        public virtual ICollection<StudentClass> Classes { get; set; } = new List<StudentClass>();
        public virtual ICollection<EmailSend> EmailSends { get; set; } = null!;
        public virtual ICollection<StudentModule> StudentModules { get; set; } = new List<StudentModule>();
        public virtual ICollection<Score> Scores { get; set; } = new List<Score>();
    }
}
