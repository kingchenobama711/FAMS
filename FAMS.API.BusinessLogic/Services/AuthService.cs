﻿using FAMS.API.BusinessLogic.Interfaces;
using FAMS.API.Helper.Dtos.Auth;
using FAMS.API.Helper.Exceptions;
using FAMS.API.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace FAMS.API.BusinessLogic.Services
{
    public class AuthService : IAuthService
    {
        private readonly IConfiguration _configuration;
        private readonly IUserService _userService;

        public AuthService(IConfiguration configuration, IUserService userService)
        {
            _configuration = configuration;
            _userService = userService;
        }
        private string CreateToken(int uid, string role)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtSettings:Key"]!));

            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new List<Claim>
            {
                new (ClaimTypes.Role, role),
                new ("uid", uid.ToString())
            };

            var token = new JwtSecurityToken(
                issuer: _configuration["JwtSettings:Issuer"],
                audience: _configuration["JwtSettings:Audience"],
                claims: claims,
                expires: DateTime.Now.AddDays(Convert.ToInt32(_configuration["JwtSettings:DurationInDays"])
                ),
                signingCredentials: credentials
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public async Task<User> GetUserByClaims(ClaimsPrincipal claims)
        {
            var userId = claims.FindFirst(c => c.Type == "uid")?.Value;

            if (userId == null)
            {
                throw new UnauthorizedException("Not found user");
            }

            var user = await _userService.GetUserById(Convert.ToInt32(userId));

            if (user == null)
            {
                throw new UnauthorizedException("Not found user");
            }

            return user;
        }

        public async Task<AuthResponseDto> Login(LoginDto body)
        {
            var user = await _userService.GetUserByEmail(body.Email);

            if (user == null || !VerifyPassword(body.Password, user.Password))
            {
                throw new UnauthorizedException("Wrong email or password");
            }

            return new AuthResponseDto()
            {
                Token = CreateToken(user.Id, user.Role)
            };
        }

        public async Task Register(RegisterDto body)
        {
            var existingUser = await _userService.GetUserByEmail(body.Email);

            if (existingUser != null)
            {
                throw new BadRequestException("Email is already used");
            }

            var hashedPassword = HashPassword(body.Password);

            await _userService.CreateUser(body.Email, hashedPassword, body.Role, body.Gender);
        }

        private string HashPassword(string password)
        {
            // Using a built-in secure hashing algorithm like SHA256
            using (var sha256 = SHA256.Create())
            {
                // Convert the input string to a byte array and compute the hash
                byte[] hashedBytes = sha256.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));

                // Convert the byte array to a hex string
                return BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
            }
        }

        private bool VerifyPassword(string enteredPassword, string storedHash)
        {
            // Hash the entered password
            string hashedPassword = HashPassword(enteredPassword);

            // Compare the hashed password with the stored hash
            return hashedPassword.Equals(storedHash);
        }
    }
}
