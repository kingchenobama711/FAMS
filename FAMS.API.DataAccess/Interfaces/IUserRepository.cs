﻿using FAMS.API.Models;

namespace FAMS.API.DataAccess.Interfaces
{
    public interface IUserRepository : IGenericRepository<User>
    {
    }
}
