﻿using FAMS.API.BusinessLogic.Interfaces;
using FAMS.API.BusinessLogic.Services;
using FAMS.API.Helper.Dtos.Auth;
using FAMS.API.Helper.Exceptions;
using FAMS.API.Models;
using Microsoft.Extensions.Configuration;
using Moq;

namespace FAMS.API.Tests.Services;

public class AuthServiceTest
{
    private readonly IAuthService _authService;

    public AuthServiceTest()
    {
        Mock<IUserService> userService = new();
        userService.Setup(service => service.GetUserByEmail("superadmin@gmail.com")).Returns(
            Task.FromResult(new User
            {
                Id = 1,
                Email = "superadmin@gmail.com",
                Password = "548d8cf86e2d301f6e1f5dc621cba2e409e8e814ba35ca1feeff6b0b995d848f",
                Gender = "Male",
                Role = "SuperAdmin"
            })!
        );

        Mock<IConfiguration> configuration = new();
        configuration.Setup(configuration => configuration["JwtSettings:Key"]).Returns("ThisIsASuperSuperSuperSecretKeyOfFamsAPI");
        configuration.Setup(configuration => configuration["JwtSettings:Issuer"]).Returns("FamsAPI");
        configuration.Setup(configuration => configuration["JwtSettings:DurationInDays"]).Returns("7");
        configuration.Setup(configuration => configuration["JwtSettings:Audience"]).Returns("FamsAPIClient");

        _authService = new AuthService(configuration.Object, userService.Object);
    }

    [Theory]
    [InlineData("superadmin@gmail.com", "Password1@")]
    public async void LoginTest(string email, string password)
    {
        // Act
        var result = await _authService.Login(new LoginDto() { Email = email, Password = password });

        // Assert
        Assert.IsType<string>(result.Token);
    }

    [Theory]
    [InlineData("superadmin@gmail.com", "1")]
    public async void LoginTest_WrongPassword(string email, string password)
    {
        try
        {
            // Act
            await _authService.Login(new LoginDto() { Email = email, Password = password });
            Assert.Fail("It should throw UnauthorizedException here");
        }
        catch (Exception e)
        {
            Assert.IsType<UnauthorizedException>(e);
            Assert.Equal("Wrong email or password", e.Message);
        }
    }
}