﻿using FAMS.API.Models;

namespace FAMS.API.BusinessLogic.Interfaces
{
    public interface IUserService
    {
        Task<User?> GetUserById(int id);
        Task<User?> GetUserByEmail(string email);
        Task<User> CreateUser(string email, string password, string role, string gender);
    }
}
