﻿namespace FAMS.API.Helper.Dtos.Program
{
    public class GetProgramDto
    {
        public int Id { get; set; }

        public required string ProgramName { get; set; }

        public string Status { get; set; } = "Active";

        public required string Code { get; set; }

        public string? Duration { get; set; } //TODO: data type under consideration

        public DateTime CreatedDate { get; set; }

        public int CreatedUserId { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedUserId { get; set; }
    }
}
