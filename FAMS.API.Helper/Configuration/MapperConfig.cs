﻿using AutoMapper;
using FAMS.API.Helper.Dtos.Classes;
using FAMS.API.Helper.Dtos.Program;
using FAMS.API.Helper.Dtos.Query;
using FAMS.API.Helper.Dtos.StudentClass;
using FAMS.API.Helper.Dtos.Students;
using FAMS.API.Helper.Dtos.Users;
using FAMS.API.Models;

namespace FAMS.API.Helper.Configuration
{
    public class MapperConfig : Profile
    {
        public MapperConfig()
        {
            CreateMap<User, GetUserDto>().ReverseMap();

            CreateMap<Student, GetStudentDto>().ReverseMap();
            CreateMap<PagedResult<Student>, PagedResult<GetStudentDto>>().ReverseMap();

            CreateMap<Class, GetClassDetailDto>().ReverseMap();

            CreateMap<StudentClass, GetStudentClassDto>().ReverseMap();

            CreateMap<Program, GetProgramDto>().ReverseMap();
        }
    }
}
