﻿using System.ComponentModel.DataAnnotations;

namespace FAMS.API.Helper.Dtos.Auth
{
    public class LoginDto
    {
        [Required]
        [EmailAddress]
        [MaxLength(150)]
        public required string Email { get; set; }

        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$", ErrorMessage = "Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character:")]
        public required string Password { get; set; }
    }
}
